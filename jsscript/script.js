//przykład przypisania zdarzenia (w naszym wypadku na kliknięcie) do elementu HTML
//bezpośrednio w JavScript (z pominięciem zdarzeń-atrybutów, jak <p onclick=""> w dokumencie HTML
//rozwiązanie to jeszcze bardziej separuje część widoku od części logicznej i wykonawczej, przez
//co stało się obecnym standardem
/*document.querySelector(".przycisk").onclick=function(zdarzenie) {
									//poniższy kod stanowi jedynie przykład deklaracji zmiennych w JS
									//		a=15	//zmienna przypisuje się do okna (window). Superglobalna, niepolecana
									//		var b = 15 //zmienna standardowa; od chwili deklaracji istnieje aż do końca bloku kodu (nawet, jeżeli zadeklarowano ją w podbloku, np. pętli)
									//		let c = 56 //zmienna lokalna, działa tylko w określonym bloku kodu (pomiędzy {})
									//		const d = -16 //stała (zawiera raz przypisaną wartość; jeżeli jest to obiekt, to stałym elementem jest "uchwyt" do niego, zaś wartości obiektu mogą być nadal zmieniane)
											const http = new XMLHttpRequest()
											http.onreadystatechange = function(wejscie) {
												
												if (http.readyState===4) {
													if (http.status===200)
													{
														document.querySelector("main").innerHTML = http.responseText
													}
												}
											}
											http.open("get","./sites/onas.html")
											http.send()
										 }											
*/
//odwołanie do elementu głównego naszej strony; często z niego korzystamy
//i bardzo nieefektywne byłoby ciągłe wynajdywanie go przez JS
const main = document.querySelector("main")

function pobierzStrone(adres, cel="main", post='', wezwijMnie) {
	//if (cel === undefined) 
	//	cel = "main"
	const http = new XMLHttpRequest()
	http.onreadystatechange = function(wejscie) {
		
		if (http.readyState===4) {
			if (http.status===200)
			{
				document.querySelector(cel).innerHTML = http.responseText
				if (wezwijMnie!== undefined) wezwijMnie(http.responseText)
			}
		}
	}
	//szablon ciągów znakowych
	http.open("post",(adres.indexOf('.php') !==-1) ? adres : `./sites/${adres}.html`)
	http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	http.send(post)
}



function zdarzenieMenu() {
	/*const przyciski = document.querySelectorAll('.przycisk')
	for (let i=0;i<przyciski.length;i++) {
		przyciski[i].onclick = () => {
			pobierzStrone(przyciski[i].dataset.strona)
			
		} 
	}*/
	document.querySelectorAll('.przycisk').forEach((wartosc) => {
		wartosc.onclick = () => {
			pobierzStrone(wartosc.dataset.strona, "main", '', function(a) {
				//podmiana adresu strony w historii przeglądarki
				history.replaceState('','',`?strona=${wartosc.dataset.strona}`)
				const przycisk = main.querySelector('.przycisk')
				if (przycisk !== null)
					przycisk.onclick = () => {
						const formularz = main.querySelector("#formularz")
						const imie = formularz.querySelector('input[name=imie]').value
						const email = formularz.querySelector('input[name=email]').value
						const temat = formularz.querySelector('select[name=temat]').value
						const tresc = btoa(encodeURIComponent(formularz.querySelector('textarea[name=tresc]').value))
						console.log(tresc)
						const zapytanie = `email=${email}&imie=${imie}&temat=${temat}&tresc=${tresc}`
						pobierzStrone("./php/form.php", "main", zapytanie)					}
			}
				)}
	})
	//podmienia historię przeglądarki przez co mamy możliwość korzystania
	//ze strony bez jej przeładowania, dodatkowo zaś uzyskujemy możliwość
	//przesłania innym osobom dokładnego adresu aktualnie przeglądanej strony
	//bez potrzeby tłumaczenia, w którą zakładkę strony mają dokałnie wejśc.
	if (location.search!=='')
		pobierzStrone(location.search.split('=')[1])
}


